from scipy.io import wavfile as wav
import numpy as np
import math
import matplotlib.pyplot as plt
# from numerical import apply_hamming_window


def calculate_energy(input_frame):
    # windowed_frame = apply_hamming_window(input_frame)
    frame_energy_samples = []
    for i in range(0, len(input_frame)):
        frame_energy_samples.append(input_frame[i]**2)
    #     print(np.sqrt(frame_energy_samples[i]))
    #     print(input_frame[i])
    # # plt.plot(input_frame,'r', frame_energy_samples,'g')
    # plt.show()
    return (np.sum(frame_energy_samples)/len(input_frame))

def find_pitch(voiced_segment):
    result = np.correlate(voiced_segment,voiced_segment, mode = 'full')
    return result[result.size/2:]
print("hello")
sampling_frequency, samples = wav.read("F:\\Study\\MSCSKE\\Project\\mscske-project\\utocorrelation\\wav\\b0265.wav")
print len(samples)
print "sampling frequency:", sampling_frequency
normalized_samples = []
samples = (samples / (1.01 * max(abs(samples))))
plt.plot(samples,'r')
plt.xlabel("Index")
plt.ylabel("Amplitude")
plt.show()
energy_list = []
for i in range(0, len(samples), 160):
    frame_1 = samples[0+i:480+i]
    energy = calculate_energy(frame_1)
    energy_list.append(energy)
plt.plot(energy_list,'r')
plt.xlabel("Frame Number")
plt.ylabel("Energy Level for the Frame")
plt.show()
print("Length of the sample" , len(samples))
print(max(energy_list))
print(energy_list.index(max(energy_list)), 'is the frame number with maximum energy')
print(len(energy_list), "is the number of frames")

guaranteed_voiced_section = samples[(energy_list.index(max(energy_list))*160):((energy_list.index(max(energy_list))*160)+480)]
plt.plot(guaranteed_voiced_section,'r')
plt.xlabel("Index")
plt.ylabel("Amplitude")
plt.show()
print(calculate_energy(guaranteed_voiced_section), "checked for same results")
auto_correltation = find_pitch(guaranteed_voiced_section)
plt.plot(auto_correltation, 'r')
plt.ylabel("Amplitude")
plt.xlabel("Index")
plt.show()
auto_correltation_modified = auto_correltation[4:len(auto_correltation)]
prominent_peak = max(auto_correltation_modified)
print auto_correltation_modified
plt.plot(auto_correltation_modified)
prominent_peak_index = auto_correltation.tolist().index(prominent_peak)
print(prominent_peak_index)
pitch_period = ((prominent_peak_index+4)*1/16000.0)
pitch = 1/pitch_period
print(pitch)