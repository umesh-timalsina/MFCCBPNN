from blaze.expr.math import floor
import numpy as np
from scipy.io.wavfile import read
from scipy import fftpack
from matplotlib import pyplot as plt
## Hamming Window. This Block is for applying a hamming window to the frame
def apply_hamming_window(frame_this):
    windowed_frame= []
    b = 0.01
    for i in range(0,len(frame_this)):
        b = frame_this[i]*(0.54-(0.46*(np.cos((2*np.pi*i/(len(frame1)-1))))))
    ## print i
        windowed_frame.append(b)
    print "Applying Hamming Window For Frame... "
    return windowed_frame
#plt.ylim(-1.2,1.2,0.1)
#plt.xlim(0,160,0.5)
#plt.plot(frame1)
#plt.show()
## This is for fft and calculating the power spectrum of the signal. we perform a 512 point fft and then discard the later 256
## samples so that we only perform computation for the lower frequencies
def frequency_domain_operations(windowed_frame):
    frame1_frequency = fftpack.fft(windowed_frame, 512)
    print "Computed FFT of 512 points for this Frame...."
    frame1_power = pow(np.abs(frame1_frequency), 2)
    print "Computed Power Spectrum of this Frame.........."
    frame1_normalized_power = frame1_power / len(frame1_power)
    print "Computed Normalized Power Spectrum of this Frame"
    plt.stem(frame1_power,'g')
    plt.xlabel("Index")
    plt.ylabel("Power")
    plt.show()
    return frame1_normalized_power
##plt.plot(frame1_power)
##plt.show()
## The mel filter bank is calculated by the following block. ##
def calculate_mfcc(spectral_power):
    mel_frequency_spectral_coefficients = []
    mel_coeffs=[]
    a,b,c = 0.0,0.0,0.0
    for j in range(1,(len(fft_samplepoints_for_melfiters)-1)):
        for i in range (0,len(spectral_power)):
            if i < fft_samplepoints_for_melfiters[j-1]:
                a = 0.0
            elif (i >= fft_samplepoints_for_melfiters[j-1] and i <= fft_samplepoints_for_melfiters[j]):
                mel_frequency_spectral_coefficients.append ((((i-fft_samplepoints_for_melfiters[j-1])/(fft_samplepoints_for_melfiters[j]-fft_samplepoints_for_melfiters[j-1]))))
            elif (i>fft_samplepoints_for_melfiters[j] and i<= fft_samplepoints_for_melfiters[j+1]):
                mel_frequency_spectral_coefficients.append((((fft_samplepoints_for_melfiters[j+1]-i)/(fft_samplepoints_for_melfiters[j+1]-fft_samplepoints_for_melfiters[j]))))
            else:
                b= 0.0
        mel_coeffs.append(sum(mel_frequency_spectral_coefficients))


    print(len(mel_coeffs))
    plt.plot(mel_frequency_spectral_coefficients,'r')
    plt.xlabel("Frequency")
    plt.ylabel("Amplitude")
    plt.show()
    #plt.plot(mel_frequency_spectral_coefficients)
    #plt.show()
    perpetual_mel_coeffs = np.log10(mel_coeffs)
    dct_result = fftpack.dct(perpetual_mel_coeffs)
    mfcc_for_this_frame = dct_result[:12]
    # print(MelFrequencyCeptralCoeffiecients)
    # plt.xlim(0,11,.5)
    # plt.ylim(-30,50,.5)
    # plt.xlabel("Number")
    # plt.ylabel("MFCC")
    # plt.plot(MelFrequencyCeptralCoeffiecients,'r')
    # plt.show()
    print "Calculated MFCCs for this frams"
    return mfcc_for_this_frame
##delta and delta-deltas computation

sampling_frequency, samples = read("F:\\Study\\MSCSKE\\Project\\mscske-project\\utocorrelation\\wav\\rai0001.wav")
#plt.plot(samples)
#plt.show()

## The Wav file read  had an amplitude that was shifted up by 127. this imposes a problem while applying hamming window.
## The block below is for normalizing the signal to have a positive and neagative amplitude.
samples_normalized = []
for a_sample in samples:
    a_sample=a_sample-127
    samples_normalized.append(a_sample)

#plt.plot(samples_normalized)
#plt.show()
emphasized_samples = [0.0]
a= 0.0
emphasized_samples[0] = samples_normalized[0]
## This block is for pre-emphasis, the pre-emphasized samples
## Pre-Emphasis is done using the following Formula - Y[n] = X[n] - X[n-1]
for i in range(1, len(samples)):
    a = (samples[i]-0.95*samples[i-1])
    emphasized_samples.append(a)

plt.plot(emphasized_samples,'r')
plt.show()

## Framing Block: This block provides frames
frame1_start = 0
melfilter_starting_frequency = (1125.0 * np.log((1+300.0/700.0)))
    #print(melfilter_starting_frequency)
melfilter_ending_frequency = (1125.0 * np.log((1+sampling_frequency/1400.0)))
    ##print (melfilter_ending_frequency)

mel_filter_points = np.linspace(melfilter_starting_frequency,melfilter_ending_frequency,28)
    ##print (mel_filter_points)

mel_filter_points_backto_hertz = (700.0 * (np.exp(mel_filter_points/1125.0)-1))
    ##print(mel_filter_points_backto_hertz)

fft_samplepoints_for_melfiters = []
a = 0.0
for i in range(0,len(mel_filter_points_backto_hertz)):
    a = np.floor((mel_filter_points_backto_hertz[i]*512.0)/sampling_frequency)
    fft_samplepoints_for_melfiters.append(a)
    ##print (fft_samplepoints_for_melfiters[len(fft_samplepoints_for_melfiters)-1])

    ## Multiplication with mel filter banks and finding the feature vectors.

mfccs_vector_for_marlon_brnado=[[]]
for i in range(0,len(emphasized_samples),110):
    frame1_end = frame1_start+480
    frame1 = emphasized_samples[frame1_start:frame1_end]       ## Since the sampling frequency is 11025 HZ. 25 msec is the ideal time period for a frame.
    #plt.ylim(-3,3)
    #plt.plot(frame1)
    #plt.show()
    windowed_frame = apply_hamming_window(frame1)
    # plt.stem(windowed_frame,'r')
    # plt.show()
    frame1_power_spectrum=frequency_domain_operations(windowed_frame)
    ## We are only considering the lower range of frequencies that is why we should be ignoring the higher power spectrum
    spectral_power = frame1_power_spectrum[:257] ## the first 257 samples are kept
    mfcc_for_this_frame = calculate_mfcc(frame1_power_spectrum)
    # print mfcc_for_this_frame
    # plt.plot(mfcc_for_this_frame)
    # plt.show()
    mfccs_vector_for_marlon_brnado.append(mfcc_for_this_frame)
    frame1_start+=110
print (mfccs_vector_for_marlon_brnado[120])
plt.plot(mfccs_vector_for_marlon_brnado[1:len(mfccs_vector_for_marlon_brnado)])
plt.show()



