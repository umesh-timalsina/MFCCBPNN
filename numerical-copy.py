from blaze.expr.math import floor
import numpy as np
from scipy.io.wavfile import read
from scipy import fftpack
from matplotlib import pyplot as plt
#import pandas as pd
#import seaborn as sns
def distance_cost_plot(distances):
    im = plt.imshow(distances,interpolation='nearest', cmap='Reds')
    plt.gca().invert_yaxis()
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.grid()
    plt.colorbar()
    plt.show()

def path_cost(x, y, accumulated_cost, distances):
    path = [[len(x)-1, len(y)-1]]
    cost = 0
    i = len(y)-1
    j = len(x)-1
    while i>0 and j>0:
        if i==0:
            j = j - 1
        elif j==0:
            i = i - 1
        else:
            if accumulated_cost[i-1, j] == min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]):
                i = i - 1
            elif accumulated_cost[i, j-1] == min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]):
                j = j-1
            else:
                i = i - 1
                j= j- 1
        path.append([j, i])
    path.append([0,0])
    for [y, x] in path:
        cost = cost +distances[x, y]
    return path, cost

def calculate_match(x,y):
    distances = np.zeros((len(y),len(x)))
    for i in range(len(y)):
        for j in range(len(x)):
            distances[i,j] = pow(np.abs((x[j]-y[i])),2)
    accumulated_cost = np.zeros((len(y), len(x)))
    accumulated_cost[0,0] = distances[0,0]
    for i in range(1,len(x)):
        accumulated_cost[0,i] = distances[0,i]+accumulated_cost[0,i-1]
    for i in range(1, len(y)):
        accumulated_cost[i,0] = distances[i, 0] + accumulated_cost[i-1, 0]
    for i in range(1, len(y)):
        for j in range(1, len(x)):
            accumulated_cost[i, j] = min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]) + distances[i, j]
    path, cost = path_cost(x,y,accumulated_cost,distances)
    path_x = [point[0] for point in path]
    path_y = [point[1] for point in path]
    plt.plot(path_x,path_y);
    distance_cost_plot(accumulated_cost)
    return path,cost

def apply_hamming_window(frame_this):
    windowed_frame= []
    b = 0.01
    for i in range(0,len(frame1)):
        b = frame1[i] * (0.54-(0.46*(np.cos((2*np.pi*i/(len(frame1)-1))))))
    ## print i
        windowed_frame.append(b)
    print "Applying Hamming Window For Frame... "
    return windowed_frame
#plt.ylim(-1.2,1.2,0.1)
#plt.xlim(0,160,0.5)
#plt.plot(frame1)
#plt.show()
## This is for fft and calculating the power spectrum of the signal. we perform a 512 point fft and then discard the later 256
## samples so that we only perform computation for the lower frequencies
def frequency_domain_operations(windowed_frame):
    frame1_frequency = fftpack.fft(windowed_frame, 512)
    print "Computed FFT of 512 points for this Frame...."
    frame1_power = pow(np.abs(frame1_frequency), 2)
    print "Computed Power Spectrum of this Frame.........."
    frame1_normalized_power = frame1_power / len(frame1_power)
    print "Computed Normalized Power Spectrum of this Frame"
    return frame1_normalized_power
##plt.plot(frame1_power)
##plt.show()
## The mel filter bank is calculated by the following block. ##
def calculate_mfcc(spectral_power):
    mel_frequency_spectral_coefficients = []
    mel_coeffs=[]
    a,b,c = 0.0,0.0,0.0
    for j in range(1,len(fft_samplepoints_for_melfiters)-1):
        for i in range (0,len(spectral_power)):
            if i < fft_samplepoints_for_melfiters[j-1]:
               mel_frequency_spectral_coefficients.append(0.0)
            elif (i >= fft_samplepoints_for_melfiters[j-1] and i <= fft_samplepoints_for_melfiters[j]):
                mel_frequency_spectral_coefficients.append (((spectral_power[i])*((i-fft_samplepoints_for_melfiters[j-1])/(fft_samplepoints_for_melfiters[j]-fft_samplepoints_for_melfiters[j-1]))))
            elif (i>fft_samplepoints_for_melfiters[j] and i<= fft_samplepoints_for_melfiters[j+1]):
                mel_frequency_spectral_coefficients.append(((spectral_power[i])*((fft_samplepoints_for_melfiters[j+1]-i)/(fft_samplepoints_for_melfiters[j+1]-fft_samplepoints_for_melfiters[j]))))
                mel_frequency_spectral_coefficients.append(((spectral_power[i])*((fft_samplepoints_for_melfiters[j+1]-i)/(fft_samplepoints_for_melfiters[j+1]-fft_samplepoints_for_melfiters[j]))))
            else:
                mel_frequency_spectral_coefficients.append(0.0)
        mel_coeffs.append(sum(mel_frequency_spectral_coefficients))
        del mel_frequency_spectral_coefficients[:]

    #print(len(mel_coeffs))
    #plt.plot(mel_coeffs)
    #plt.show()

    perpetual_mel_coeffs = np.log10(mel_coeffs)
    dct_result = fftpack.dct(perpetual_mel_coeffs)
    mfcc_for_this_frame = dct_result[:12]
    # print(MelFrequencyCeptralCoeffiecients)
    # plt.xlim(0,11,.5)
    # plt.ylim(-30,50,.5)
    # plt.xlabel("Number")
    # plt.ylabel("MFCC")
    # plt.plot(MelFrequencyCeptralCoeffiecients,'r')
    # plt.show()
    print "Calculated MFCCs for this frams"
    return mfcc_for_this_frame
##delta and delta-deltas computation

sampling_frequency, samples = read("F:\Study\MSCSKE\Project\mscske-project\code-python\Waves\\bluefox83-20080413-bwq\wav\\rai0001.wav")
#sampling_frequency_to_match, samples_to_match = read("F:\Study\MSCSKE\Project\mscske-project\code-python\Waves\\bluefox83-20080413-bwq\wav\\rai0002.wav")
# plt.plot(samples)
# plt.xlabel("Time")
# plt.ylabel("samples")
# plt.show()

## The Wav file read  had an amplitude that was shifted up by 127. this imposes a problem while applying hamming window.
## The block below is for normalizing the signal to have a positive and neagative amplitude.
samples_normalized = []
samples_to_be_matched_normalized = []
for a_sample in samples:
    a_sample=a_sample-127
    samples_normalized.append(a_sample)
# for a_sample in samples_to_match:
#     a_sample= a_sample-127
#     samples_to_be_matched_normalized.append(a_sample)
#plt.plot(samples_normalized)
#plt.show()
emphasized_samples = [0.0]
emphasized_samples_to_be_matched = [0.0]
a= 0.0
emphasized_samples[0] = samples[0]
# emphasized_samples_to_be_matched[0] = samples_to_be_matched_normalized[0]
## This block is for pre-emphasis, the pre-emphasized samples
## Pre-Emphasis is done using the following Formula - Y[n] = X[n] - X[n-1]
for i in range(1, len(samples_normalized)):
    a = (samples[i]-0.95*samples[i-1])
    emphasized_samples.append(a)
# for i in range(1,len(samples_to_be_matched_normalized)):
#     a = (samples_to_be_matched_normalized[i] - 0.95 * samples_to_be_matched_normalized[i-1])
#     emphasized_samples_to_be_matched.append(a)
# plt.plot(emphasized_samples)
# plt.show()

## Framing Block: This block provides frames
frame1_start = 0
melfilter_starting_frequency = (1125.0 * np.log((1+300.0/700.0)))
    #print(melfilter_starting_frequency)
melfilter_ending_frequency = (1125.0 * np.log((1+5512.5/700.0)))
    ##print (melfilter_ending_frequency)

mel_filter_points = np.linspace(melfilter_starting_frequency,melfilter_ending_frequency,28)
    ##print (mel_filter_points)

mel_filter_points_backto_hertz = (700.0 * (np.exp(mel_filter_points/1125.0)-1))
    ##print(mel_filter_points_backto_hertz)

fft_samplepoints_for_melfiters = []
a = 0.0
for i in range(0,len(mel_filter_points_backto_hertz)):
    a = np.floor((mel_filter_points_backto_hertz[i]*512.0)/sampling_frequency)
    fft_samplepoints_for_melfiters.append(a)
    ##print (fft_samplepoints_for_melfiters[len(fft_samplepoints_for_melfiters)-1])

    ## Multiplication with mel filter banks and finding the feature vectors.

mfccs_vector_for_this_wav=[[]]
# mfccs_vector_for_wav_to_be_matched = [[]]
for i in range(0,len(emphasized_samples)/160):
    frame1_end = frame1_start+400
    frame1 = emphasized_samples[frame1_start:frame1_end]       ## Since the sampling frequency is 16000 HZ. 25 msec is the ideal time period for a frame.
    #plt.ylim(-3,3)
    # plt.stem(frame1)
    # plt.show()
    windowed_frame = apply_hamming_window(frame1)
    frame1_power_spectrum=frequency_domain_operations(frame1)
    ## We are only considering the lower range of frequencies that is why we should be ignoring the higher power spectrum
    # plt.stem(windowed_frame)
    # plt.show()
    spectral_power = frame1_power_spectrum[:257] ## the first 257 samples are kept
    mfcc_for_this_frame = calculate_mfcc(spectral_power)
    # print mfcc_for_this_frame
    # plt.plot(mfcc_for_this_frame)
    # plt.show()
    mfccs_vector_for_this_wav.append(mfcc_for_this_frame)
    frame1_start+=160
# for i in range(0,len(emphasized_samples_to_be_matched)/160):
#     frame1_end = frame1_start+400
#     frame1 = emphasized_samples_to_be_matched[frame1_start:frame1_end]       ## Since the sampling frequency is 16000 HZ. 25 msec is the ideal time period for a frame.
#     #plt.ylim(-3,3)
#     #plt.plot(frame1)
#     #plt.show()
#     windowed_frame = apply_hamming_window(frame1)
#     frame1_power_spectrum=frequency_domain_operations(frame1)
#     ## We are only considering the lower range of frequencies that is why we should be ignoring the higher power spectrum
#     spectral_power = frame1_power_spectrum[:257] ## the first 257 samples are kept
#     mfcc_for_this_frame = calculate_mfcc(spectral_power)
#     # print mfcc_for_this_frame
#     # plt.plot(mfcc_for_this_frame)
#     # plt.show()
#     mfccs_vector_for_wav_to_be_matched.append(mfcc_for_this_frame)
#     frame1_start+=160

##print (mfccs_vector_for_this_wav[6])
plt.stem(mfccs_vector_for_this_wav[30])
plt.show()
plt.stem(mfccs_vector_for_this_wav[99])
plt.show()
x=np.asarray(mfccs_vector_for_this_wav[100])
y=np.asarray(mfccs_vector_for_this_wav[222])
print(len(x))
print(len(y))
path,cost = calculate_match(x,y)
print("The Optimal cost is given as:")
print(path)
print("The Optimal cost is given as:")
print(cost)
