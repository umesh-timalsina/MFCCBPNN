from features import mfcc
from features import logfbank
import numpy as np
import scipy.io.wavfile as wav
from matplotlib import pyplot as plt


def distance_cost_plot(distances):
    im = plt.imshow(distances,interpolation='nearest', cmap='Reds')
    plt.gca().invert_yaxis()
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.grid()
    plt.colorbar()
    plt.show()

def path_cost(x, y, accumulated_cost, distances):
    path = [[len(x)-1, len(y)-1]]
    cost = 0
    i = len(y)-1
    j = len(x)-1
    while i>0 and j>0:
        if i==0:
            j = j - 1
        elif j==0:
            i = i - 1
        else:
            if accumulated_cost[i-1, j] == min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]):
                i = i - 1
            elif accumulated_cost[i, j-1] == min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]):
                j = j-1
            else:
                i = i - 1
                j= j- 1
        path.append([j, i])
    path.append([0,0])
    for [y, x] in path:
        cost = cost +distances[x, y]
    return path, cost

def calculate_match(x,y):
    distances = np.zeros((len(y),len(x)))
    for i in range(len(y)):
        for j in range(len(x)):
            distances[i,j] = pow(np.abs((x[j]-y[i])),2)
    accumulated_cost = np.zeros((len(y), len(x)))
    accumulated_cost[0,0] = distances[0,0]
    for i in range(1,len(x)):
        accumulated_cost[0,i] = distances[0,i]+accumulated_cost[0,i-1]
    for i in range(1, len(y)):
        accumulated_cost[i,0] = distances[i, 0] + accumulated_cost[i-1, 0]
    for i in range(1, len(y)):
        for j in range(1, len(x)):
            accumulated_cost[i, j] = min(accumulated_cost[i-1, j-1], accumulated_cost[i-1, j], accumulated_cost[i, j-1]) + distances[i, j]
    path, cost = path_cost(x,y,accumulated_cost,distances)
    path_x = [point[0] for point in path]
    path_y = [point[1] for point in path]
    plt.plot(path_x,path_y);
    distance_cost_plot(accumulated_cost)
    return path,cost

(rate_normal, sig_normal) = wav.read("F:\Study\\MSCSKE\Project\\mscske-project\\utocorrelation\wav\\rai0001.wav")
plt.plot(sig_normal, 'r')
plt.xlabel("Index")
plt.ylabel("Amplitude")
plt.show()
emphasized_samples=[]
for i in range(1, len(sig_normal)):
    a = (sig_normal[i]-0.95*sig_normal[i-1])
    emphasized_samples.append(a)

plt.plot(emphasized_samples,'r')
plt.xlabel("Index")
plt.ylabel("Amplitude")
plt.show()
frame1 = emphasized_samples[:480]
plt.stem(frame1, 'r')
plt.xlabel("Index")
plt.ylabel("Amplitude")
plt.show()

# mfcc_feat_pathelogical = mfcc(sig_pathelogical,rate_pathelogical)
mfcc_feat_normal= mfcc(sig_normal,rate_normal)
plt.plot(mfcc_feat_normal[0], 'r', mfcc_feat_normal[1],'g')
plt.xlabel("MFCC coefficient Number")
plt.ylabel("Corresponding MFCC Value")
plt.show()
x=np.asarray(mfcc_feat_normal[100])
y=np.asarray(mfcc_feat_normal[222])
print(len(x))
print(len(y))
path,cost = calculate_match(x,y)
print("The Optimal path is given as:")
print(path)
print("The Optimal cost is given as:")
print(cost)
