from sklearn import datasets
from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader
import numpy as np
import matplotlib.pyplot as plt
olivetti = datasets.fetch_olivetti_faces()
X, y = olivetti.data, olivetti.target
ds = ClassificationDataSet(4,1, nb_classes=3)

for i in range(len(X)):
    ds.addSample(X[i],y[i])

trndata, partdata = ds.splitWithProportion(0.6)
tstdata,validdata = ds.splitWithProportion(0.5)
# trndata._converToOneofMany()
# tstdata._convertToOneofMany()
# validdata._converToOneofMany()
net = buildNetwork(4,3,3, outclass=SoftmaxLayer)
trainer = BackpropTrainer(net, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)
trnerr, valeerr = trainer.trainUntilConvergence(dataset=trndata, maxEpochs=50)
plt.plot(trnerr,'b',valeerr,'r')